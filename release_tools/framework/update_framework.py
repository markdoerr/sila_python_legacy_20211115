#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*compile_sila*

:details: This script updates all framework files that are extracted from the
          `sila\\_base <https://gitlab.com/SiLA2/sila_base>`_  repository. Therefore it creates a local clone of the
          repository and then copies the relevant files into the sila library.

:file:    update_framework.py
:authors: Timm Severin

:date: (creation)          2019-08-28
:date: (last modification) 2019-08-29
________________________________________________________________________
"""
# Disable not that important messages
# pylint: disable=broad-except, invalid-name

import logging

import tempfile
import os
import filecmp
import shutil
import stat

# Import git package just to download the sila_base git. Requires GitPython package
import git

# CONFIG START
SILA_BASE_URL = 'https://gitlab.com/SiLA2/sila_base.git'
SILA2LIB_FRAMEWORK_PATH = os.path.join(
    os.path.dirname(__file__),
    '..', '..',
    'sila_library', 'sila2lib', 'framework'
)

# CONFIG END

# logging details
logging.basicConfig(format='%(levelname)s | %(module)s.%(funcName)s: %(message)s', level=logging.INFO)

dir_temp = tempfile.mkdtemp(prefix='sila_base.', suffix='.git')

logging.info('Cloning sila_base repository from "{url}" into {dir}'.format(url=SILA_BASE_URL, dir=dir_temp))
sila_base_repo = git.Repo.clone_from(url=SILA_BASE_URL, to_path=dir_temp)
sila_base_head = sila_base_repo.head.commit
logging.info('Latest commit on sila_base repository: {commit}'.format(commit=sila_base_head.hexsha))

# counter for how many changes were made
count_updates = 0

# read the current commit stored in this repository
sila2lib_framework_commit_file = os.path.join(SILA2LIB_FRAMEWORK_PATH, '.sila_base_commit_hash')
try:
    with open(sila2lib_framework_commit_file, 'r') as file:
        sila2lib_framework_head = file.readline().strip()
except FileNotFoundError:
    sila2lib_framework_head = None

# check if the sila_base commit is different from the currently stored one
if sila2lib_framework_head == str(sila_base_head.hexsha):
    logging.info('All files up to date')
    exit(0)

logging.info('Checking protobuf files:')
sila_base_proto = [file for file in os.listdir(os.path.join(dir_temp, 'protobuf')) if file[-6:] == '.proto']

for proto_file in sila_base_proto:
    logging.info('\t{filename}'.format(filename=proto_file))
    # First figure out if we want to update it
    input_file = os.path.join(dir_temp, 'protobuf', proto_file)
    output_file = os.path.join(SILA2LIB_FRAMEWORK_PATH, 'protobuf', proto_file)
    try:
        if filecmp.cmp(f1=input_file, f2=output_file, shallow=False):
            logging.info('\t\tFile already up-to-date')
            continue
    except FileNotFoundError:
        # that is okay, we will just copy it
        logging.info('\t\tFile new to sila2lib')
    except Exception as err:
        logging.error(
            'An unknown error occurred while trying to copy the file. '
            'Skipping the update for this file.'
        )
        continue

    # copy the file
    try:
        shutil.copy2(src=input_file, dst=output_file)
    except Exception as err:
        logging.error('\t\tUpdate...failed (copy error: {err})'.format(err=err))
    else:
        count_updates += 1
        logging.info('\t\tUpdate...successful')


logging.info('Checking schema files:')
sila_base_schema = [file for file in os.listdir(os.path.join(dir_temp, 'schema')) if file[-4:] == '.xsd']
for schema_file in sila_base_schema:
    logging.info('\t{filename}'.format(filename=schema_file))

    # First figure out if we want to update it
    input_file = os.path.join(dir_temp, 'schema', schema_file)
    output_file = os.path.join(SILA2LIB_FRAMEWORK_PATH, 'schema', schema_file)
    try:
        if filecmp.cmp(f1=input_file, f2=output_file, shallow=False):
            logging.info('\t\tFile already up-to-date')
            continue
    except FileNotFoundError:
        # that is okay, we will just copy it
        logging.info('\t\tFile new to sila2lib')
    except Exception as err:
        logging.error(
            'An unknown error occurred while trying to copy the file. '
            'Skipping the update for this file.'
        )
        continue

    # copy the file
    try:
        shutil.copy2(src=input_file, dst=output_file)
    except Exception as err:
        logging.error('\t\tUpdate...failed (copy error: {err})'.format(err=err))
    else:
        count_updates += 1
        logging.info('\t\tUpdate...successful')

logging.info('Checking standard features:')
sila_base_features = [
    file for file in os.listdir(os.path.join(dir_temp, 'feature_definitions', 'org', 'silastandard', 'core'))
    if file[-9:] == '.sila.xml']
for feature_file in sila_base_features:
    logging.info('\t{filename}'.format(filename=feature_file))

    # First figure out if we want to update it
    input_file = os.path.join(dir_temp, 'feature_definitions', 'org', 'silastandard', 'core', feature_file)
    output_file = os.path.join(SILA2LIB_FRAMEWORK_PATH, 'feature_definitions', 'org.silastandard', feature_file)
    try:
        if filecmp.cmp(f1=input_file, f2=output_file, shallow=False):
            logging.info('\t\tFile already up-to-date')
            continue
    except FileNotFoundError:
        # that is okay, we will just copy it
        logging.info('\t\tFile new to sila2lib')
    except Exception as err:
        logging.error(
            'An unknown error occurred while trying to copy the file. '
            'Skipping the update for this file.'
        )
        continue

    # copy the file
    try:
        shutil.copy2(src=input_file, dst=output_file)
    except Exception as err:
        logging.error('\t\tUpdate...failed (copy error: {err})'.format(err=err))
    else:
        count_updates += 1
        logging.info('\t\tUpdate...successful')

logging.info('Number of files updated: {number}'.format(number=count_updates))
if count_updates > 0:
    logging.warning('Files were updated, consider regenerating the sila2lib framework files '
                    'and updating the sila2lib version.')

# Store the current commit version in the repo file
try:
    with open(sila2lib_framework_commit_file, 'w') as file:
        file.write(str(sila_base_head.hexsha))
        file.write('\n')
        file.write('\n'.join([
            '# This is the commit hash from the sila_base repository which was last used to update the framework files',
            '#   DO NOT MODIFY unless you know what you are doing',
            '#   To update the framework files, please use the `update_framework.py` release_tool.'
        ]))
except Exception as err:
    logging.warning('Failed to update the commit storage in sila2lib framework.')
else:
    logging.info('Updated the commit information in sila2lib framework to the current head of sila_base.')


# Cleanup
sila_base_repo.close()
del sila_base_repo


def remove_readonly(func, path, _):
    """Function that fixes problems with readonly files when deleting the temporary git repository."""
    os.chmod(path, stat.S_IWRITE)
    func(path)


try:
    shutil.rmtree(dir_temp, onerror=remove_readonly)
except PermissionError:
    logging.error('Failed to delete temporary directory {dir}. Consider deleting it manually.'.format(dir=dir_temp))

logging.info('Cleanup finished.')
