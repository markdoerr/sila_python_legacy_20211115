# Authors and Contributors to sila_python

* Maximilian Schulz
* Sebastian Hans
* Robert Giessmann
* Shaon Debnath
* Timm Severin
* Lukas Bromig
* Florian Meinicke
* Stefan Born
* Thorsten Gressling
* Mark Doerr (*mark.doerr at uni-greifswald dot de*)
