# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: CoverController.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='CoverController.proto',
  package='sila2.org.silastandard.instruments.covercontroller.v1',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x15\x43overController.proto\x12\x35sila2.org.silastandard.instruments.covercontroller.v1\x1a\x13SiLAFramework.proto\"\x16\n\x14OpenCover_Parameters\"\x15\n\x13OpenCover_Responses\"\x17\n\x15\x43loseCover_Parameters\"\x16\n\x14\x43loseCover_Responses2\xe6\x02\n\x0f\x43overController\x12\xa6\x01\n\tOpenCover\x12K.sila2.org.silastandard.instruments.covercontroller.v1.OpenCover_Parameters\x1aJ.sila2.org.silastandard.instruments.covercontroller.v1.OpenCover_Responses\"\x00\x12\xa9\x01\n\nCloseCover\x12L.sila2.org.silastandard.instruments.covercontroller.v1.CloseCover_Parameters\x1aK.sila2.org.silastandard.instruments.covercontroller.v1.CloseCover_Responses\"\x00\x62\x06proto3')
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_OPENCOVER_PARAMETERS = _descriptor.Descriptor(
  name='OpenCover_Parameters',
  full_name='sila2.org.silastandard.instruments.covercontroller.v1.OpenCover_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=101,
  serialized_end=123,
)


_OPENCOVER_RESPONSES = _descriptor.Descriptor(
  name='OpenCover_Responses',
  full_name='sila2.org.silastandard.instruments.covercontroller.v1.OpenCover_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=125,
  serialized_end=146,
)


_CLOSECOVER_PARAMETERS = _descriptor.Descriptor(
  name='CloseCover_Parameters',
  full_name='sila2.org.silastandard.instruments.covercontroller.v1.CloseCover_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=148,
  serialized_end=171,
)


_CLOSECOVER_RESPONSES = _descriptor.Descriptor(
  name='CloseCover_Responses',
  full_name='sila2.org.silastandard.instruments.covercontroller.v1.CloseCover_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=173,
  serialized_end=195,
)

DESCRIPTOR.message_types_by_name['OpenCover_Parameters'] = _OPENCOVER_PARAMETERS
DESCRIPTOR.message_types_by_name['OpenCover_Responses'] = _OPENCOVER_RESPONSES
DESCRIPTOR.message_types_by_name['CloseCover_Parameters'] = _CLOSECOVER_PARAMETERS
DESCRIPTOR.message_types_by_name['CloseCover_Responses'] = _CLOSECOVER_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

OpenCover_Parameters = _reflection.GeneratedProtocolMessageType('OpenCover_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _OPENCOVER_PARAMETERS,
  '__module__' : 'CoverController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.covercontroller.v1.OpenCover_Parameters)
  })
_sym_db.RegisterMessage(OpenCover_Parameters)

OpenCover_Responses = _reflection.GeneratedProtocolMessageType('OpenCover_Responses', (_message.Message,), {
  'DESCRIPTOR' : _OPENCOVER_RESPONSES,
  '__module__' : 'CoverController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.covercontroller.v1.OpenCover_Responses)
  })
_sym_db.RegisterMessage(OpenCover_Responses)

CloseCover_Parameters = _reflection.GeneratedProtocolMessageType('CloseCover_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _CLOSECOVER_PARAMETERS,
  '__module__' : 'CoverController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.covercontroller.v1.CloseCover_Parameters)
  })
_sym_db.RegisterMessage(CloseCover_Parameters)

CloseCover_Responses = _reflection.GeneratedProtocolMessageType('CloseCover_Responses', (_message.Message,), {
  'DESCRIPTOR' : _CLOSECOVER_RESPONSES,
  '__module__' : 'CoverController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.instruments.covercontroller.v1.CloseCover_Responses)
  })
_sym_db.RegisterMessage(CloseCover_Responses)



_COVERCONTROLLER = _descriptor.ServiceDescriptor(
  name='CoverController',
  full_name='sila2.org.silastandard.instruments.covercontroller.v1.CoverController',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=198,
  serialized_end=556,
  methods=[
  _descriptor.MethodDescriptor(
    name='OpenCover',
    full_name='sila2.org.silastandard.instruments.covercontroller.v1.CoverController.OpenCover',
    index=0,
    containing_service=None,
    input_type=_OPENCOVER_PARAMETERS,
    output_type=_OPENCOVER_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='CloseCover',
    full_name='sila2.org.silastandard.instruments.covercontroller.v1.CoverController.CloseCover',
    index=1,
    containing_service=None,
    input_type=_CLOSECOVER_PARAMETERS,
    output_type=_CLOSECOVER_RESPONSES,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_COVERCONTROLLER)

DESCRIPTOR.services_by_name['CoverController'] = _COVERCONTROLLER

# @@protoc_insertion_point(module_scope)
