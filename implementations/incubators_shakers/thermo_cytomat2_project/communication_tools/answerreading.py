#!/usr/bin/python3

import serial
ser = serial.Serial("/dev/ttyUSB0", 9600, timeout=1)

#file to analyse answer

"""
def bs_reader() :
	Part1 = answer[:2]
	Part2 = answer[3]
	Part3 = answer[4]
	Listecode = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
	Listeerreur = ["0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"]
	if Part1 == "bs" :
		i=0
		while Part2 != Listecode[i] : 
			i+=1
		Part2 = Listeerreur[i]
		i=0
		while Part3 != Listecode[i] :
			i+=1
		Part3 = Listeerreur[i]
		
		if Part2[0] == '1' :
			print("Transfert station occupied")
			Transfert = 1
		else : 
			print("Transfert station free")
			Transfert = 0
		if Part2[1] == '1' :
			print("Main door open")
			Maindoor = 1
		else : 
			print("Main door closed")
			Mainsdoor = 0
		if Part2[2] == 1 :
			print("Automatic door open")
			Automatic=1
		else : 
			print("Automatic door closed")
			Automatic=0
		if Part2[3] == '1' :
			print("Handler occupied")
			Handler = 1
		else : 
			print("Handler unoccupied")
			Handler = 1
		
		if Part3[0] == '1' :
			print("Register error")
		if Part3[1] == '1' :
			print("Register warning")
		if Part3[2] == 1 :
			print("Not Ready")
		if Part3[2] == '0' and Part3[3] == '0' :
			print("Ready")
			X=1
		if Part3[3] == '1' :
			print("Busy")
	return(X)"""

def bs_reader() :
	X=0
	while X != 1 :
		ser.write("ch:bs\r".encode())
		answer = ser.readline()
		answer = answer.decode()
		Part1 = answer[0:2]
		Part2 = answer[3]
		Part3 = answer[4]
		Listecode = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
		Listeerreur = ["0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"]
		if Part1 == "bs" :
			i=0
			while Part2 != Listecode[i] :
				i+=1
			Part2 = Listeerreur[i]
			i=0
			while Part3 != Listecode[i] :
				i+=1
			Part3 = Listeerreur[i]
			if Part2[0] == '1' :
				#print("Transfert station occupied")
				bs_reader.Transfert = 1
			else : 
				#print("Transfert station free")
				bs_reader.Transfert = 0
				global Tranfert
			if Part2[1] == '1' :
				#print("Main door open")
				bs_reader.Maindoor = 1
			else : 
				#print("Main door closed")
				bs_reader.Mainsdoor = 0
			if Part2[2] == '1' :
				#print("Automatic door open")
				bs_reader.Automatic=1
			else : 
				#print("Automatic door closed")
				bs_reader.Automatic=0
			if Part2[3] == '1' :
				#print("Handler occupied")
				bs_reader.Handler = 1
			else : 
				#print("Handler unoccupied")
				bs_reader.Handler = 1
			if Part3[2] == '0' and Part3[3] == '0' :
				print("Ready")
				X = 1
			if Part3[3] == '1' :
				print("Busy")


def er_reader():
	Part1 = answer[:2]
	Part2 = answer[3:]
	if Part1 == "er" :
		if Part2 == "01" :
			print("System occupied, command not accepted")
		elif Part2 == "02" :
			print("Unknown command")
		elif Part2 == "03" :
			print("Command syntax error")
		elif Part2 == "04" :
			print("Wrong command parameter")
		elif Part2 == "05" :
			print("Stacker number does not exist")
		elif Part2 == "11" :
			print("Handler wrong position")
		elif Part2 == "12" :
			print("Command impossible du to the tongue position")
		elif Part2 == "21" :
			print("Handler occupied")
		elif Part2 == "22" :
			print("Handler empty")
		elif Part2 == "31" :
			print("Transfert station empty")
		elif Part2 == "32" :
			print("Transfert station occupied")
		elif Part2 == "41" :
			print("Automatic door not configure")
		elif Part2 == "42" :
			print("Automatic door closed")
		elif Part2 == "51" :
			print("Error in intern memory access")
		elif Part2 == "52" :
			print("Wrong password/unauthorized access")

def bw_reader():
	Part1 = answer[:2]
	Part2 = answer[3:]
	if Part1 == "bw" :
		if Part2 == "01" :
			print("Communication to motor disrupted")
		elif Part2 == "02" :
			print("Plate not charged on the handler/tongue")
		elif Part2 == "03" :
			print("Plate not discharged on the handler/tongue")
		elif Part2 == "04" :
			print("Tongue not extended")
		elif Part2 == "05" :
			print("Process time-out")
		elif Part2 == "06" :
			print("Automatic door not open")
		elif Part2 == "07" :
			print("Automatic door not closed")
		elif Part2 == "08" :
			print("Tongue not retracted")
		elif Part2 == "09" :
			print("Initialisation because of the main door opening")
		elif Part2 == "0C" :
			print("Transfer station not turned")


def be_reader():
	Part1 = answer[:2]
	Part2 = answer[3:]
	if Part1 == "be" :
		if Part2 == "01" :
			print("Communication to motor disrupted")
		elif Part2 == "02" :
			print("Plate not charged on the handler/tongue")
		elif Part2 == "03" :
			print("Plate not discharged on the handler/tongue")
		elif Part2 == "04" :
			print("Tongue not extended")
		elif Part2 == "05" :
			print("Process time-out")
		elif Part2 == "06" :
			print("Automatic door not open")
		elif Part2 == "07" :
			print("Automatic door not closed")
		elif Part2 == "08" :
			print("Tongue not retracted")
		elif Part2 == "09" :
			print("Initialisation because of the main door opening")
		elif Part2 == "0A" :
			print("Motor temperature to high")
		elif Part2 == "0B" :
			print("Error in the motor")
		elif Part2 == "0C" :
			print("Transfer station not turned")
		elif Part2 == "0D" :
			print("Communication to heater/CO2 regulator disrupted")
		elif Part2 == "FF" :
			print("Error in the error system")


