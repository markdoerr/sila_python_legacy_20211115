**Error-Codes**

*  [PSS](1. Terms) sends an [Error-Code](2.2 Error-Codes) to the [PS](1. Terms) if a command is declined
*  [Error-Codes](2.2 Error-Codes) can't be queried

| Value | Error message |
| ------ | ------ |
| 0x01 | Device is busy, new command is declined |
| 0x02 | Command unknown | 
| 0x03 | Error in telegram structure | 
| 0x04 | Wrong parameters in telegram |
| 0x05 | Unknown [Stock-Position](1. Terms) specified |
| 0x11 | Wrong [Handler](1. Terms) position |
| 0x12 | Command not possible, because [Shovel](1. Terms) is extended |
| 0x21 | [Handler](1. Terms) already occupied |
| 0x22 | [Handler](1. Terms) is empty |
| 0x31 | [Transfer-Station](1. Terms) empty |
| 0x32 | [Transfer-Station](1. Terms) occupied |
| 0x33 | [Transfer-Station](1. Terms) not in position |
| 0x41 | [Automatic-Lift-Door](1. Terms) not configurated |
| 0x42 | [Automatic-Lift-Door](1. Terms) not open |
| 0x51 | Error ocurred while accessing the internal memory |
| 0x52 | Wrong password / unauthorized access |

**Command:**

Telegram from [PS](1. Terms) to [PSS](1. Terms): `mv:ts` 

Response from [PSS](1. Terms) to [PS](1. Terms): `er **`

**Example:**

![image](uploads/9cf382a2b18f22497bbca524a374cde4/image.png)

*  [PS](1. Terms) send a command to transport a [Micro-Test-Plate](1. Terms) from the [Transfer-Station](1. Terms) to [Stacker](1. Terms)-2 on position 53
*  Only 42 [Pitches](1. Terms) are configured &#x21D2; Position 53 can't be reached &#x21D2; Error 0x05




