**Swap-Station-Register**

*  Can only be queried, if [Swap-Station](1. Terms) is configured
*  Our Cytomat-2 has a standard [Transfer-Station](1. Terms) and no [Swap-Station](1. Terms)
*  The [Swap-Station-Register](2.6 Swap-Station-Register) saves the position of the [Swap-Station](1. Terms) and the occupancy of the two plates

**Command**

Telegram from [PS](1. Terms) to [PSS](1. Terms): `ch:sw`

Response from [PSS](1. Terms) to [PS](1. Terms): `sw ***`

**Meaning of the three ASCII letters**

| Value | 0 | 1 | 2 |
| ------ | ------ | ------ | ------ |
| Position of [Swap-Station](1. Terms) (1. letter) | - |  Plate 1 is in front of the [Automatic-Lift-Door](1. Terms) |  Plate 2 is in front of the [Automatic-Lift-Door](1. Terms) | 
| Occupancy of plate in front of [Automatic-Lift-Door](1. Terms) (2. letter)| Not Occupied with a [Micro-Test-Plate](1. Terms) | Occupied with a [Micro-Test-Plate](1. Terms) | - |
| Occupancy of plate at [PS](1. Terms) (3. latter) | not occupied with [Micro-Test-Plate](1. Terms) |occupied with a [Micro-Test-Plate](1. Terms) | - |

**Example**

![image](uploads/f975adcba755e7ab396cf96d3a5f0a85/image.png)

&#x21D2; Plate 2 is in front of the [Automatic-Lift-Door](1. Terms)

&#x21D2; Plate 2 is not occupied with a [Micro-Test-Plate](1. Terms)

&#x21D2; Plate 1, which is directed to the [PS](1. Terms), is occupied with a [Micro-Test-Plate](1. Terms)



