**Action-Register**

*  The current movement-steps of the [PSS](1. Terms) are saved to the [Action-Register](2.5 Action-Register)
*  The byte of the [Action-Register](2.5 Action-Register) contains two information: 
      *  1.  The target of the movement (first three bits)
         2.  The type of movement (last five bits)

**Current movement- or control-command**

| Value | Meaning |
| ------ | ------ |
| 0x01 | Movement, height-motor to [Stock-Position](1. Terms) (minus offset) |
| 0x02 | Query, if height-position is reached (minus offset) |
| 0x03 | Movement, height-motor to [Stock-Position](1. Terms) (plus offset) |
| 0x04 | Query, if height-position is reached (plus offset) |
| 0x05 | Movement, rotation-motor to [Stock-Position](1. Terms) |
| 0x06 | Query, if rotate-position is reached |
| 0x07 | Movement, extend [Shovel](1. Terms) |
| 0x08 | Query, if [Shovel](1. Terms) is extended |
| 0x09 | Query, if end-switch of [Shovel](1. Terms) is extended |
| 0x0a | Movement, retract [Shovel](1. Terms) |
| 0x0b | Query, if [Shovel](1. Terms) is retracted |
| 0x0c | Close [Automatic-Lift-Door](1. Terms) |
| 0x0d | Query, if [Automatic-Lift-Door](1. Terms) is closed |
| 0x0e | Open [Automatic-Lift-Door](1. Terms) |
| 0x0f | Query, if [Automatic-Lift-Door](1. Terms) is open |
| 0x10 | [Transfer-Station](1. Terms) in position 1 |
| 0x11 | Query, if [Transfer-Station](1. Terms) is in position 1 |
| 0x12 | [Transfer-Station](1. Terms) in position 2 |
| 0x13 | Query, if [Transfer-Station](1. Terms) is in position 2 |
| 0x14 | test plate on [Shovel](1. Terms) |
| 0x15 | test plate on [Transfer-Station](1. Terms) |
| 0x16 | Move to position [Barcode-Reader](1. Terms) |
| 0x17 | Test position [Barcode-Reader](1. Terms) |
| 0x18 | read barcode |

**Target of movement**

| Value | Meaning |
| ------- | -------|
| 0x10 | Movement target: [Init-Position](1. Terms) |
| 0x20 | Movement target: [Wait-Position](1. Terms) |
| 0x40 | Movement target: [Stacker](1. Terms) |
| 0x80 | Movement target: [Transfer-Station](1. Terms) |

**Command**

Telegram from [PS](1. Terms) to [PSS](1. Terms): `ch:ba`

Response from [PSS](1. Terms) to [PS](1. Terms): `ba**`

**Example**

![image](uploads/6e12def358aa05ac4cad0e53be7a76ae/image.png)


*  [Handler](1. Terms) moves to the [Stacker](1. Terms) and tests, if a [Micro-Test-Plate](1. Terms) is mounted on the [Shovel](1. Terms)
*  0x74 (Hex) &#x21D2; 0111 0100 (bin)
*  Target position = 011 &#x21D2; 0x03 &#x21D2; Target = [Stacker](1. Terms) (Maybe error in documentation, because 0x03 is not 0x40)
*  Movement type = 1 0100 &#x21D2; 0x14 &#x21D2; test [Micro-Test-Plate](1. Terms) on [Shovel](1. Terms)
