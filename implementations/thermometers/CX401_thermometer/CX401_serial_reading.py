"""
________________________________________________________________________

:PROJECT:

*Thermometer Elmetron CX-401 serial connection test*

:details: : Thermometer Elmetron CX-401
          
:file:    CX401_connection_test.py

:author:  mark doerr <mark@MicT660p> : contrib.

:version: 0.0.1

:date: (creation)          20190919
:date: (last modification) 20190919
.. note:: some remarks
.. todo:: -

________________________________________________________________________

"""

import time

import serial

from sila2lib.com.serial_com import listOpenPorts, Connection

def main():
    open_ports = listOpenPorts()

    #conn = Connection(port=open_ports[0], baudrate=9600, verbose=True)
    #conn.open()

    port = serial.Serial(open_ports[0], baudrate=9600, timeout=1)
    #~ logging.debug("Trying to open serial port ")
    port.reset_input_buffer()

    try:
        while True:

            rcv_arr = []

            rcv = b""

            while len(rcv) < 1:
                rcv = port.readline()
                rcv_arr = rcv.split()

            value = str(rcv_arr[2], 'unicode_escape') # rcv_arr[2].decode('unicode_escape') # str(rcv, 'ascii') #rcv_arr[0].decode('unicode_escape') #.replace(unit, "")
            print(value)

            #temp_line = conn.readline()
            #print( temp_line.decode('unicode_escape') )
            # rcv_arr = rcv.split()
            #  value = rcv_arr[1].decode('unicode_escape').replace(unit, "")

            time.sleep(1)

    except KeyboardInterrupt:
        #conn.close()
        port.close()
        print("Thermometer read ended")

if __name__ == '__main__':
    """Main: """
    main()
