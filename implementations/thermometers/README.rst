
SiLA 2 thermometer implementations
===================================

Arduino based thermistor thermometer (not observable)
------------------------------------------------------

This example illustrates a very simple Arduino based
thermometer. The actual temperatues are sent via
serial interface to the SiLA server. The current temperature can be retrieved by
a single SiLA remote call.


Arduino based thermistor thermometer (observable)
--------------------------------------------------

This implementation uses the above Arduino based thermometer, but now
one can monitor temperature over a certain time interval.

CX401 Thermometer
------------------

CX401 is a multi purpose lab device (temperature, voltage, pH, ...).
It is connected to a computer/raspberry pi via an USB-serial interface.
This implementation shows how to transform it into a SiLA 2 device. 


SimpleThermometer
------------------

Simple SiLA thermometer application skeleton.
