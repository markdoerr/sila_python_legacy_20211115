Manual installation procedure (LINUX)
=====================================

The manual installation is only recommended for experienced users. Make
sure your virtual environment is set-up and activated.

SiLA Library
^^^^^^^^^^^^

The following commands will install the SiLA2 library in the currently
active python path:

.. code:: bash

    cd sila_library
    pip3 install -r requirements_base.txt
    pip3 install .

    cd ..

Once the installation has completed, it can be imported into any python
code using the statement

.. code:: python

    import sila2lib

SiLA Tools (Code generator)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The SilA tools are not necessarily required to run a SiLA client/server.
However, they are quite useful in developing both. To install, run the
following commands

.. code:: bash

    # install SiLA tool requirements
    cd sila_tools
    pip3 install -r requirements.txt

    # install the code generator
    cd sila2codegenerator
    pip3 install .
    cd ..

    cd ..

Create a directory for config files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Client/server applications use ``.conf`` files to store information.
They are written to ``$HOME/.config/sila2``. This directory should be
created during the installation:

|linux| In Linux:

.. code:: bash

    mkdir -p ~/.config/sila2

|windows| In Windows:

.. code:: console

    mkdir %userprofile%\.config\sila2

In case you prefer a manual installation, please run the following
commands:

::

    git clone https://gitlab.com/SiLA2/sila_python
    cd sila_python
    #
    cd sila_library
    pip install .
    cd ..
    #
    cd sila_tools/sila2codegenerator
    pip install .
    cd ../..
    #
    mkdir -p ~/.config/sila2
