# Changelog

## 0.2.0 (2019-06-28)

SiLA2CodeGenerator arguments:
 * List of templates in the package is available
 * Template can be chosen from command line
 
Template
 * New **default** template *run-methods* added 

## 0.1.1 (2019-06-27)

Initial (so far) functioning version