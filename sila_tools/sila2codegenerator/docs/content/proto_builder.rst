ProtoBuilder Module
=======================================

.. automodule:: SiLA2CodeGenPackage.proto_builder

ProtoBuilder Class
---------------------------------------

.. autoclass:: SiLA2CodeGenPackage.proto_builder.ProtoBuilder
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__

CompileFormats Enum
---------------------------------------

.. autoclass:: SiLA2CodeGenPackage.proto_builder.CompileFormats
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__
